How to build
=====================================

1. Install vscode for your distribution https://code.visualstudio.com/docs/setup/linux
2. Install Platformio Extension
3. Be patient and let platformio Installer do its thing.
4. reload vscode
5. click the home-button on the lower blue bar. the platformio home site opens
6. Use the "Open Project" button IN the platformio home site