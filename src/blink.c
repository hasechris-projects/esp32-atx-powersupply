/* Blink Example

   This example code is in the Public Domain (or CC0 licensed, at your option.)

   Unless required by applicable law or agreed to in writing, this
   software is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
   CONDITIONS OF ANY KIND, either express or implied.
*/
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/queue.h"
#include "driver/gpio.h"
#include "sdkconfig.h"

#define GPIO_OUTPUT_IO_0    2
#define GPIO_OUTPUT_IO_1    5
#define GPIO_OUTPUT_PIN_SEL  ( (1ULL<<GPIO_OUTPUT_IO_0) | (1ULL<<GPIO_OUTPUT_IO_1) )

#define GPIO_INPUT_IO_0     4
#define GPIO_INPUT_PIN_SEL  (1ULL<<GPIO_INPUT_IO_0)
#define ESP_INTR_FLAG_DEFAULT 0
#define DEBOUNCETIME 3

int GPIO_ATX_ON = 0;
volatile int numberOfButtonInterrupts = 0;
volatile bool lastState;
volatile uint32_t debounceTimeout = 0;
portMUX_TYPE mux = portMUX_INITIALIZER_UNLOCKED;

static void IRAM_ATTR gpio_isr_handler(void* arg)
{
    uint32_t gpio_num = (uint32_t) arg;
    portENTER_CRITICAL_ISR(&mux);
    numberOfButtonInterrupts++;
    lastState = gpio_get_level(gpio_num);
    debounceTimeout = xTaskGetTickCount(); //version of millis() that works from interrupt
    portEXIT_CRITICAL_ISR(&mux);
}

void app_main()
{
    gpio_config_t io_conf;
    //disable interrupt
    io_conf.intr_type = GPIO_INTR_DISABLE;
    //set as output mode
    io_conf.mode = GPIO_MODE_OUTPUT;
    //bit mask of the pins that you want to set,e.g.GPIO18/19
    io_conf.pin_bit_mask = GPIO_OUTPUT_PIN_SEL;
    //disable pull-down mode
    io_conf.pull_down_en = 0;
    //disable pull-up mode
    io_conf.pull_up_en = 0;
    //configure GPIO with the given settings
    gpio_config(&io_conf);

    //interrupt of falling edge
    io_conf.intr_type = GPIO_INTR_DISABLE;
    //bit mask of the pins, use GPIO4/5 here
    io_conf.pin_bit_mask = GPIO_INPUT_PIN_SEL;
    //set as input mode    
    io_conf.mode = GPIO_MODE_INPUT;
    //enable pull-up mode
    io_conf.pull_up_en = 1;
    gpio_config(&io_conf);
    
    //change gpio intrrupt type for one pin
    gpio_set_intr_type(GPIO_INPUT_IO_0, GPIO_INTR_NEGEDGE);

    //install gpio isr service
    gpio_install_isr_service(ESP_INTR_FLAG_DEFAULT);
    //hook isr handler for specific gpio pin
    gpio_isr_handler_add(GPIO_INPUT_IO_0, gpio_isr_handler, (void*) GPIO_INPUT_IO_0);

    //printf("Debounced ButtonRead Task running on core %d\n", xPortGetCoreID());
    
    uint32_t saveDebounceTimeout;
    bool saveLastState;
    int save;

    //int cnt = 0;
    while(1) {
        portENTER_CRITICAL_ISR(&mux);
        save  = numberOfButtonInterrupts;
        saveDebounceTimeout = debounceTimeout;
        saveLastState  = lastState;
        portEXIT_CRITICAL_ISR(&mux);
        bool currentState = gpio_get_level(GPIO_INPUT_IO_0);

        if ((save != 0) && (currentState == saveLastState) && ((esp_timer_get_time() / 1000) - saveDebounceTimeout > DEBOUNCETIME )) {
            if (currentState == 0) {
                printf("Button is pressed and debounced, current tick=%lld\n", (esp_timer_get_time() / 1000) );
                switch (GPIO_ATX_ON)
                {
                case 0:
                    //vTaskDelay(250 / portTICK_RATE_MS);
                    gpio_set_level(GPIO_OUTPUT_IO_0, 0);
                    gpio_set_level(GPIO_OUTPUT_IO_1, 1);
                    printf("ATX Power Supply an...\t");
                    printf("GPIO_LEVEL: %d\n" , gpio_get_level(GPIO_INPUT_IO_0));
                    GPIO_ATX_ON = 1;
                    //vTaskDelay(3000 / portTICK_RATE_MS);
                    break;
                case 1:
                    vTaskDelay(3000 / portTICK_RATE_MS);
                    if (gpio_get_level(GPIO_INPUT_IO_0) == 0) {
                        gpio_set_level(GPIO_OUTPUT_IO_0, 1);
                        gpio_set_level(GPIO_OUTPUT_IO_1, 0);
                        printf("ATX Power Supply aus...\t");
                        printf("GPIO_LEVEL: %d\n" , gpio_get_level(GPIO_INPUT_IO_0));
                        GPIO_ATX_ON = 0;
                        vTaskDelay(1500 / portTICK_RATE_MS);
                    }
                    break;
                }
            } else {
                printf("Button is released and debounced, current tick=%lld\n", (esp_timer_get_time() / 1000) );
            }

            printf("Button Interrupt Triggered %d times, current State=%u, time since last trigger %lldms\n", save, currentState, (esp_timer_get_time() / 1000) - saveDebounceTimeout);

            portENTER_CRITICAL_ISR(&mux); // can't change it unless, atomic - Critical section
            numberOfButtonInterrupts = 0; // acknowledge keypress and reset interrupt counter
            portEXIT_CRITICAL_ISR(&mux);
            vTaskDelay(10 / portTICK_PERIOD_MS);
        }
        vTaskDelay(10 / portTICK_PERIOD_MS);        
    }
}